## Źródło
Kod źródłowy znajduje się w src/main/scala/SimpleApp.scala

## Uruchamianie
Program tworzy paczkę .jar za pomocą polecenia ./sbt package (załączone w źródłach)  
Następnie spark-submit PATH-TO-PROJECT/target/scala-2.11/simple-project_2.11-1.0.jar
