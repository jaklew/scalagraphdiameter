package simpleApp

import org.apache.spark.sql.SparkSession
import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.lib
import org.apache.spark._
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
	

object SimpleApp {
  def main(args: Array[String]) {
	val spark = SparkSession.builder.appName("SimpleApp").getOrCreate()
	
    println(Console.GREEN + "===================================================STARTING CALCULATIONS==================================================")
    print(Console.RESET)
    val output = MyGraphDiameter.calculate(spark)
    println(Console.GREEN + "===================================================FINISHED CALCULATIONS==================================================")
    print(Console.RESET)
    spark.stop()
    println(Console.GREEN + "===================================================PROGRAM OUTPUT==================================================")
    print(Console.RESET)
    println(Console.RED + Console.BLINK + Console.BOLD + s"Diameter found: $output")
    print(Console.RESET)
    println(Console.GREEN + "===============================================END OF PROGAM OUTPUT================================================")
	println(Console.RESET)
  }
}


//utility funkcje dla map
object MapUtils{
  type GraphMap = Map[VertexId, Int]
  // funkcja która tworzy mapę z zestawu argumentów
  // metoda wywoływania: createMapFromManyArguments(id1->1, id2->2, ... i tak ile potrzebujemy kluczy z wartościami)
  def createMapFromManyArguments(x: (VertexId, Int)*) = Map(x : _*)
  
  //funkcja zwiększająca wszystkie wartości dla wszystkich kluczy w mapie o 1
  def incrementAllValuesInMap(graphMap: GraphMap): GraphMap = graphMap.map { case (vertex,distance) => vertex -> (distance + 1) }
  
  //funkcja tworząca sumę dwóch map, ustawiająca wartość dla klucza na min(map1[klucz], map2[klucz[)
  def sumTwoMaps(map1: GraphMap, map2: GraphMap): GraphMap = {
	(map1.keySet ++ map2.keySet).map {
		key => key -> math.min(map1.getOrElse(key, Int.MaxValue), map2.getOrElse(key, Int.MaxValue))
	}(collection.breakOut) 
	//breakOut to komponent paczki scala.collection, pozwalający na małą optymalizację tworzenia mapy (bez tworzenia i trzymania
	//w pamięci pośrednich obiektów),
	//przekazywany jako argument, a zwracający CanBuildFrom[From, T, To], który z kolei jest abstrakcją która wie, jaki element
	//zbudować, z jakich elementów, i za pomocą jakich parametrów tych elementów - brzmi skomplikowanie, ale w prostych przypadkach
	//wystarczy zastosować collection.breakOut i przestać się martwić
  }
}

object MyGraphDiameter{
	type GraphMap = Map[VertexId, Int]
	def calculate(spark: SparkSession) : Int = {
		//definicja wierzchołków: każdy przechowuje swoje id i mapę z id wszystkich innych wierzchołków, wartości 
		//- odległość do wierzchołka o danym kluczu
		val vertices = Array((1L, Map()),(2L, Map()), (3L, Map()), (4L, Map()))
		val verticeRDD = spark.sparkContext.parallelize(vertices)
		//definicja krawędzi: każda to id dwóch wierzchołków, i waga
		val edges = Array(Edge(1L, 2L, 1), Edge(2L, 3L, 1), Edge(2L, 4L, 1))
		val edgeRDD = spark.sparkContext.parallelize(edges)
		val graph = Graph(verticeRDD, edgeRDD)
		
		//stan początkowy algorytmu: każdy wierzchołek wie tylko o sobie samym, ilu ma sąsiadów, i że odległość do siebie samego to 0
		val initialGraphState = graph.mapVertices{ (verticeId, _) => MapUtils.createMapFromManyArguments(verticeId -> 0) }
		
		//pregel. 
		//korzystam z PregelAPI (z graphX), ogólnie rzecz biorąc jest to abstrakcja służąca do iteratywnego, synchronicznego (!), zrównoleglonego
		//wysyłania wiadomości, ograniczona do topologii grafu (jego wymiarów, między innymi). pregel operuje w seriach kroków, w których
		//wierzchołki otrzymują sumę wiadomości przychodzących, kalkulują nową wartość swojej własności(tutaj - mapy odległości),
		//oraz na końcu wysyłają kolejną wiadomość do wszystkich sąsiadów. w kroku w którym kończy się wysyłanie wiadomości (żaden
		//z wierzchołków nie zmienia swojego stanu, pregel kończy działanie.
		// jako argumenty przyjmuje wiadomość początkową, oraz trzy funkcje: odbierającą wiadomości, tworzącą wiadomości, i łączącą wiadomości na poziomie wierzchołka.
		val preg = initialGraphState.pregel(MapUtils.createMapFromManyArguments())(
			//f-cja odbierająca wiadomości, wywołuje się po f-cji łączącej
			(vertexId, mapA, mapB) => MapUtils.sumTwoMaps(mapA,mapB),
			//f-cja tworząca wiadomości
			(triple) => {
				val sourceMapIncremented = MapUtils.incrementAllValuesInMap(triple.dstAttr)
				val destinationMapIncremented = MapUtils.incrementAllValuesInMap(triple.srcAttr)
				
				//to jest zwracana wartość z tej lambdy (w Scali nie ma returnów, trochę jak w Visual Basicu, za to 
				//zwracane jest ostatnie wywołanie
				//ify zabezpieczają warunek wyjścia z pregela: jeżeli suma map po inkrementacji i z poprzedniego stanu się nie zmienia,
				// nic nie jest wysyłane
				List(
					if(triple.srcAttr != MapUtils.sumTwoMaps(sourceMapIncremented, triple.srcAttr)) Some((triple.srcId, sourceMapIncremented)) else None,
					if(triple.dstAttr != MapUtils.sumTwoMaps(destinationMapIncremented, triple.dstAttr)) Some((triple.dstId, destinationMapIncremented)) else None
				).flatten.toIterator
			},
			//f-cja łącząca wiadomości, wywołuje się na początku każdej iteracji
			(mapA, mapB) => MapUtils.sumTwoMaps(mapA, mapB)
		)
		var max_distance = Int.MinValue
		preg.vertices.collect.foreach(i => {max_distance = math.max(max_distance, i._2.values.max)} )
		max_distance
	}
}
